﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ferdinand.Core.Features.MyWorkouts;
using MediatR;

namespace Ferdinand.API.Controllers
{
    public class DeleteSetController : ApiController
    {
        private readonly IMediator _mediator;

        public DeleteSetController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public HttpResponseMessage Post(SetDeleteCommand command)
        {
            if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var model = _mediator.Send(command);
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }
    }
}
