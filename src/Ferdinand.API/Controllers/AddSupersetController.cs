﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ferdinand.Core.Features.Workouts.Supersets;
using Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets;
using MediatR;

namespace Ferdinand.API.Controllers
{
    public class AddSupersetController : ApiController
    {
        private readonly IMediator _mediator;

        public AddSupersetController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public HttpResponseMessage Post(SupersetAddCommand command)
        {
            if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var model = _mediator.Send(command);
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }
    }
}
