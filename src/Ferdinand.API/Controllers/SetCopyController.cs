﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets;
using MediatR;

namespace Ferdinand.API.Controllers
{
    public class SetCopyController : ApiController
    {
        private readonly IMediator _mediator;

        public SetCopyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public HttpResponseMessage Post(SetCopyCommand command)
        {
            if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var model = _mediator.Send(command);
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }
    }
}
