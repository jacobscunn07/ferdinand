﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets;
using MediatR;

namespace Ferdinand.API.Controllers
{
    public class SetDeleteController : ApiController
    {
        private readonly IMediator _mediator;

        public SetDeleteController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public HttpResponseMessage Post(SetDeleteCommand command)
        {
            if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var model = _mediator.Send(command);
            return Request.CreateResponse(model ? HttpStatusCode.OK : HttpStatusCode.BadRequest, model);
        }
    }
}
