﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using MediatR;

namespace Ferdinand.API.Controllers
{
    public class WorkoutsController : ApiController
    {
        private readonly IMediator _mediator;

        public WorkoutsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, new {blah = "blah"});
        }
    }
}
