﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets;
using MediatR;

namespace Ferdinand.API.Controllers
{
    public class SetController : ApiController
    {
        private readonly IMediator _mediator;

        public SetController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public HttpResponseMessage Post(SetAddCommand command)
        {
            if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var model = _mediator.Send(command);
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }


        public HttpResponseMessage Delete(SetDeleteCommand command)
        {
            if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            var model = _mediator.Send(command);
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }
    }
}
