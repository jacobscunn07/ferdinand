﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Ferdinand.API.Filters
{
    public class ApiKeyAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            IEnumerable<string> values = new List<string>();
            actionContext.Request.Headers.TryGetValues("X-ApiKey", out values);
            if (values != null)
            {
                return;
            }

            var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.NotAcceptable,
                "Missing or incorrect X-ApiKey header");
            actionContext.Response = response;
        }
    }
}