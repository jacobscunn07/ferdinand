﻿using System.Linq;
using System.Web;
using Ferdinand.Core.Features.Security;
using Ferdinand.Core.Security;
using MediatR;

namespace Ferdinand.API.DependencyResolution
{
    public class UserSession : IUserSession
    {
        private readonly IMediator _mediator;

        public UserSession(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IUser GetCurrentUser()
        {
            var headers = HttpContext.Current.Request.Headers;
            var apiTokenHeaders = headers.GetValues("X-Ferdy-ApiToken") ?? Enumerable.Empty<string>();
            var token = apiTokenHeaders.FirstOrDefault() ?? string.Empty;
            var userInfo = _mediator.Send(new UserInfoByTokenQuery { Token = token });
            return new UserInfo(userInfo.Id, userInfo.EmailAddress, userInfo.AccessLevel)
            {
                FirstName = userInfo.FirstName,
                LastName = userInfo.LastName
            };
        }

        public bool IsAuthenticated()
        {
            return (HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated;
        }
    }
}