﻿--Person
INSERT INTO Person (Id, Email, EmailConfirmed, PasswordSalt, PasswordHash, PhoneNumber, PhoneNumberConfirmed, FirstName, LastName, AccessLevelId, Token, CreatedDateTime, ModifiedDateTime) 
VALUES ('56DC672C-59DA-4638-9C98-D8BA5845EEE4', 'admin@admin.com', 1, '7dc38522c555478990f9654a9115530a', '/rPq42Cp7SUVK7fJb9SBKybVjHg=', '1234567890', 1, 'Power', 'User', '1', newid(), getdate(), getdate());
--Exercise
INSERT INTO Exercise (Id, Name, Description, CreatedDateTime, ModifiedDateTime)
VALUES ('D7ECDFD7-B8B7-444D-8D4D-F555789C4A79', 'Bench Press', 'Flat bench press', getdate(), getdate())
--Workout
INSERT INTO Workout (Id, PersonId, Name, Description, CreatedDateTime, ModifiedDateTime) 
VALUES('A178CF0A-EB7F-4516-A2CB-B66F05165657', '56DC672C-59DA-4638-9C98-D8BA5845EEE4', 'Workout 1', 'Push', getdate(), getdate());
--Workout Superset
INSERT INTO WorkoutSuperset (Id, WorkoutId, CreatedDateTime, ModifiedDateTime)
VALUES ('D1549549-7FDA-457C-ACF1-F168DD2C5419', 'A178CF0A-EB7F-4516-A2CB-B66F05165657', getdate(), getdate());
--Workout Superset Exercise
INSERT INTO WorkoutSupersetExercise (Id, WorkoutSupersetId, ExerciseId, Name, IsCustomExercise, CreatedDateTime, ModifiedDateTime)
VALUES ('10864CD5-B7FF-4F69-96F2-6D03FAB5F87E', 'D1549549-7FDA-457C-ACF1-F168DD2C5419', 'D7ECDFD7-B8B7-444D-8D4D-F555789C4A79', 'Flat Barbell Bench Press', 0, getdate(), getdate());
--Workout Superset Exercise Set
INSERT INTO WorkoutSupersetExerciseSet (Id, WorkoutSupersetExerciseId, CreatedDateTime, ModifiedDateTime)
VALUES ('EFBBFF7F-7BF6-4073-B4C7-9C6C2E5A2EBF', '10864CD5-B7FF-4F69-96F2-6D03FAB5F87E', getdate(), getdate());
--Attribute
INSERT INTO Attribute (Id, Name, DataType, CreatedDateTime, ModifiedDateTime)
VALUES ('8043DD5D-EFD0-4121-9551-B36294AE8285', 'Goal Reps', 'Number', getdate(), getdate())
INSERT INTO Attribute (Id, Name, DataType, CreatedDateTime, ModifiedDateTime)
VALUES ('BFA4D835-E7CE-476B-BC1B-66EDE5B5C195', 'Actual Reps', 'Number', getdate(), getdate())
INSERT INTO Attribute (Id, Name, DataType, CreatedDateTime, ModifiedDateTime)
VALUES ('5B9299F5-329F-44C5-AA93-F9150E368FFD', 'Weight', 'Number', getdate(), getdate())
----Workout Superset Exercise Set Attribute
INSERT INTO WorkoutSupersetExerciseSetAttribute (Id, WorkoutSupersetExerciseSetId, AttributeId, Value, CreatedDateTime, ModifiedDateTime)
VALUES ('181EB839-A11F-4728-8773-424FDBEC1448', 'EFBBFF7F-7BF6-4073-B4C7-9C6C2E5A2EBF', '8043DD5D-EFD0-4121-9551-B36294AE8285', '8', getdate(), getdate())
INSERT INTO WorkoutSupersetExerciseSetAttribute (Id, WorkoutSupersetExerciseSetId, AttributeId, Value, CreatedDateTime, ModifiedDateTime)
VALUES ('491BAF58-AA90-43A7-8687-D1C70A504EB7', 'EFBBFF7F-7BF6-4073-B4C7-9C6C2E5A2EBF', 'BFA4D835-E7CE-476B-BC1B-66EDE5B5C195', '0', getdate(), getdate())
INSERT INTO WorkoutSupersetExerciseSetAttribute (Id, WorkoutSupersetExerciseSetId, AttributeId, Value, CreatedDateTime, ModifiedDateTime)
VALUES ('6923C374-4450-42E7-8C80-E41276F70EE7', 'EFBBFF7F-7BF6-4073-B4C7-9C6C2E5A2EBF', '5B9299F5-329F-44C5-AA93-F9150E368FFD', '0', getdate(), getdate())