﻿CREATE TABLE list.AccessLevel (
	Id INT NOT NULL
		CONSTRAINT PK_AccessLevel_Id PRIMARY KEY
	, Name NVARCHAR(256) NOT NULL)

INSERT INTO list.AccessLevel VALUES (1, 'Admin')
INSERT INTO list.AccessLevel VALUES (2, 'Trainer')
INSERT INTO list.AccessLevel VALUES (3, 'User')