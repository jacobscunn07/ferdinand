﻿using Ferdinand.Core.Entities;
using System.Data.Entity;

namespace Ferdinand.Core.DataAccess
{
    public class FerdinandContext : DbContext, IDatabase
    {
        public FerdinandContext()
            : base("Ferdinand")
        {
        }

        public DbSet<Person> People { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExerciseCategory> ExerciseCategories { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<TemplateSuperset> TemplateSupersets { get; set; }
        public DbSet<TemplateSupersetExercise> TemplateSupersetExercises { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<WorkoutSuperset> WorkoutSupersets { get; set; }
        public DbSet<WorkoutSupersetExercise> WorkoutSupersetExercises { get; set; }
        public DbSet<WorkoutSupersetExerciseSet> WorkoutSupersetExerciseSets { get; set; }


        public static FerdinandContext Create()
        {
            return new FerdinandContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(typeof(FerdinandContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}
