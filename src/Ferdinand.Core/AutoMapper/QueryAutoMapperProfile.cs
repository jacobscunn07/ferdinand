﻿using System.Linq;
using AutoMapper;
using Ferdinand.Core.Features.Workouts;

namespace Ferdinand.Core.AutoMapper
{
    public class QueryAutoMapperProfile : Profile
    {
        protected override void Configure()
        {
            // Feature: My Workouts
            // My Workouts
            CreateMap<Entities.Workout, MyWorkoutsModel.Workout>();
            //.ForMember(dest => dest.WorkoutSupersets, opt => opt.Ignore());
            CreateMap<Entities.WorkoutSuperset, MyWorkoutsModel.WorkoutSuperset>();
            CreateMap<Entities.WorkoutSupersetExercise, MyWorkoutsModel.WorkoutSupersetExercise>();

            //Entities to Dtos 
            CreateMap<Entities.Workout, Dtos.WorkoutDto>()
                .ForMember(dest => dest.Supersets,
                    opt => opt.MapFrom(src => src.WorkoutSupersets));
            CreateMap<Entities.WorkoutSuperset, Dtos.WorkoutSupersetDto>()
                .ForMember(dest => dest.SupersetExercises,
                    opt => opt.MapFrom(src => src.WorkoutSupersetExercises));
            CreateMap<Entities.WorkoutSupersetExercise, Dtos.WorkoutSupersetExerciseDto>()
                .ForMember(dest => dest.SupersetExerciseSets,
                    opt => opt.MapFrom(src => src.WorkoutSupersetExerciseSets));
            CreateMap<Entities.WorkoutSupersetExerciseSet, Dtos.WorkoutSupersetExerciseSetDto>()
                .ForMember(dest => dest.SupersetExerciseSetAttributes,
                    opt => opt.MapFrom(src => src.WorkoutSupersetExerciseSetAttributes));
            CreateMap<Entities.WorkoutSupersetExerciseSetAttribute, Dtos.WorkoutSupersetExerciseSetAttributeDto>()
                .ForMember(dest => dest.AttributeDataType, opt => opt.MapFrom(src => src.Attribute.DataType))
                .ForMember(dest => dest.AttributeName, opt => opt.MapFrom(src => src.Attribute.Name));
        }
    }
}