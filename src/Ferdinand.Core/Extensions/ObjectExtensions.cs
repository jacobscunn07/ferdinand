﻿using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ferdinand.Core.Extensions
{
    public static class ObjectExtensions
    {
        public static string ToJson(this object obj, bool useEnumerationDisplay = false)
        {
            var jsonSerializer =
                JsonSerializer.Create(new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            //if (useEnumerationDisplay)
            //{
            //    jsonSerializer.Converters.Add(new EnumerationDisplayConverter());
            //}

            var stringWriter = new StringWriter();
            jsonSerializer.Serialize(stringWriter, obj);

            return stringWriter.ToString();
        }
    }
}
