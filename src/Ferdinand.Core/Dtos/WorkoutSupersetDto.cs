﻿using System;
using System.Collections.Generic;

namespace Ferdinand.Core.Dtos
{
    public class WorkoutSupersetDto
    {
        public Guid Id { get; set; }
        public IList<WorkoutSupersetExerciseDto> SupersetExercises { get; set; }
    }
}