﻿using System;
using System.Collections.Generic;

namespace Ferdinand.Core.Dtos
{
    public class WorkoutDto
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IList<WorkoutSupersetDto> Supersets { get; set; }
    }
}