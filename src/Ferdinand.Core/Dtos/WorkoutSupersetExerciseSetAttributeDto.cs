﻿using System;

namespace Ferdinand.Core.Dtos
{
    public class WorkoutSupersetExerciseSetAttributeDto
    {
        public Guid Id { get; set; }
        public string AttributeDataType { get; set; }
        public string AttributeName { get; set; }
        public string Value { get; set; }
    }
}