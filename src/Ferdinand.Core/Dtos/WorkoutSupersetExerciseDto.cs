﻿using System;
using System.Collections.Generic;

namespace Ferdinand.Core.Dtos
{
    public class WorkoutSupersetExerciseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsCustomExercise { get; set; }
        public IList<WorkoutSupersetExerciseSetDto> SupersetExerciseSets { get; set; }
    }
}