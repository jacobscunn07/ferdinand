﻿using System;
using System.Collections.Generic;

namespace Ferdinand.Core.Dtos
{
    public class WorkoutSupersetExerciseSetDto
    {
        public Guid Id { get; set; }
        public IList<WorkoutSupersetExerciseSetAttributeDto> SupersetExerciseSetAttributes { get; set; }
    }
}