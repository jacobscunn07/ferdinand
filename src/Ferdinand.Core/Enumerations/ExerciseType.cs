﻿using Yay.Enumerations;

namespace Ferdinand.Core.Enumerations
{
    public class ExerciseType : Enumeration<ExerciseType>
    {
        public static readonly ExerciseType BodyBuilding = new ExerciseType(1, "Body Building");

        public ExerciseType(int value, string displayName)
            : base(value, displayName)
        {
        }
    }
}