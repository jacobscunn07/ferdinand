﻿using Yay.Enumerations;

namespace Ferdinand.Core.Enumerations
{
    public class AccessLevel : Enumeration<AccessLevel>
    {
        public static readonly AccessLevel Admin = new AccessLevel(1, "Admin");
        public static readonly AccessLevel Trainer = new AccessLevel(2, "Trainer");
        public static readonly AccessLevel User = new AccessLevel(3, "User");

        public AccessLevel(int value, string displayName)
            : base(value, displayName)
        {
        }
    }
}