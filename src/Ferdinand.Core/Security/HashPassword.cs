﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Ferdinand.Core.Security
{
    public class HashPassword : IEncrypt
    {
        public string Encrypt(string password, string salt)
        {
            return Hash(password, salt);
        }

        private static string Hash(string password, string salt)
        {
            var bytes = Encoding.Unicode.GetBytes(password);
            var src = Convert.FromBase64String(salt);
            var dst = new byte[src.Length + bytes.Length];

            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);

            var algorith = HashAlgorithm.Create("SHA1");
            var inArray = algorith.ComputeHash(dst);

            return Convert.ToBase64String(inArray);
        }
    }
}
