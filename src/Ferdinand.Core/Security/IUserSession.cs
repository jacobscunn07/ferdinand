﻿namespace Ferdinand.Core.Security
{
    public interface IUserSession
    {
        IUser GetCurrentUser();
        bool IsAuthenticated();
    }
}