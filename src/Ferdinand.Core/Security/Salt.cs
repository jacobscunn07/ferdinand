﻿using System;

namespace Ferdinand.Core.Security
{
    public class Salt : ISalt
    {
        public string NewSalt()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}