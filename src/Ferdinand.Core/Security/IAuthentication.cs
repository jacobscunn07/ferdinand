﻿namespace Ferdinand.Core.Security
{
    public interface IAuthentication
    {
        void Login(string username, bool rememberMe = false);
        void Logout();
    }
}
