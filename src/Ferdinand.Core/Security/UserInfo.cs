﻿using Ferdinand.Core.Enumerations;
using System;

namespace Ferdinand.Core.Security
{
    public class UserInfo : IUser
    {
        public UserInfo(Guid id, string emailAddress, AccessLevel accessLevel)
        {
            Id = id;
            EmailAddress = emailAddress;
            AccessLevel = accessLevel;
        }

        public Guid Id { get; private set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public AccessLevel AccessLevel { get; set; }
        public Guid Token { get; set; }

        public string FullName
        {
            get
            {
                return String.Format("{0}{1}{2}", FirstName,
                    (String.IsNullOrWhiteSpace(FirstName) || String.IsNullOrWhiteSpace(LastName)) ? String.Empty : " ",
                    LastName);
            }
        }
    }
}
