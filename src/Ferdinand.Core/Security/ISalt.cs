﻿namespace Ferdinand.Core.Security
{
    public interface ISalt
    {
        string NewSalt();
    }
}
