﻿namespace Ferdinand.Core.Security
{
    public interface IEncrypt
    {
        string Encrypt(string password, string salt);
    }
}
