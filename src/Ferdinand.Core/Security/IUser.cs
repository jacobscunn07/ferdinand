﻿using Ferdinand.Core.Enumerations;
using System;

namespace Ferdinand.Core.Security
{
    public interface IUser
    {
        Guid Id { get; }
        string EmailAddress { get; }
        string FirstName { get; }
        string LastName { get; }
        string FullName { get; }
        AccessLevel AccessLevel { get; }
        Guid Token { get; set; }
    }
}
