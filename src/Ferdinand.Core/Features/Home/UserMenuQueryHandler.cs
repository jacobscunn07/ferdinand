﻿using MediatR;
using Ferdinand.Core.Security;

namespace Ferdinand.Core.Features.Home
{
    public class UserMenuQueryHandler : IRequestHandler<UserMenuQuery, UserMenuModel>
    {
        private readonly IUserSession _userSession;

        public UserMenuQueryHandler(IUserSession userSession)
        {
            _userSession = userSession;
        }

        public UserMenuModel Handle(UserMenuQuery message)
        {
            var model = new UserMenuModel();
            if(_userSession.IsAuthenticated())
                model.UserName = _userSession.GetCurrentUser().FullName;
            return model;
        }
    }
}
