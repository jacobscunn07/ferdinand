﻿using FluentValidation;

namespace Ferdinand.Core.Features.Exercises
{
    public class ExerciseAddCommandValidator : AbstractValidator<ExerciseAddCommand>
    {
        public ExerciseAddCommandValidator()
        {
            RuleFor(c => c.Name)
                .NotEmpty()
                .WithMessage("Name property is required");

            RuleFor(c => c.Description)
                .NotEmpty()
                .WithMessage("Description property is required");
        }
    }
}