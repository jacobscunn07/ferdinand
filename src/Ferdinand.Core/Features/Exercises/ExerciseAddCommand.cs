﻿using Ferdinand.Core.Enumerations;
using MediatR;
using System;

namespace Ferdinand.Core.Features.Exercises
{
    public class ExerciseAddCommand : IRequest<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ExerciseType ExerciseType { get; set; }
    }
}