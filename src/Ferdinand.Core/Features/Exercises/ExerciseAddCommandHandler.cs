﻿using System;
using MediatR;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Entities;

namespace Ferdinand.Core.Features.Exercises
{
    public class ExerciseAddCommandHandler : IRequestHandler<ExerciseAddCommand, Guid>
    {
        private readonly FerdinandContext _ctx;

        public ExerciseAddCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public Guid Handle(ExerciseAddCommand command)
        {
            var exercise = new Exercise
            {
                Name = command.Name,
                Description = command.Description
            };

            //_ctx.Exercises.Add(exercise);

            //_ctx.SaveChanges();

            return exercise.Id;
        }
    }
}