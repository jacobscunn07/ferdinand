﻿using System.Linq;
using Ferdinand.Core.DataAccess;
using MediatR;

namespace Ferdinand.Core.Features.Security
{
    public class UserInfoQueryHandler : IRequestHandler<UserInfoQuery, UserInfoModel>
    {
        private readonly FerdinandContext _ctx;

        public UserInfoQueryHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public UserInfoModel Handle(UserInfoQuery message)
        {
            var person = _ctx.People.SingleOrDefault(x => x.Email.Equals(message.EmailAddress));

            return new UserInfoModel
            {
                AccessLevel = person.AccessLevel,
                EmailAddress = person.Email,
                FirstName = person.FirstName,
                LastName = person.LastName,
                Id = person.Id,
                Token = person.Token
            };
        }
    }
}
