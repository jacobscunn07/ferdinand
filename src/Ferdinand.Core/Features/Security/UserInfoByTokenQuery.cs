﻿using MediatR;

namespace Ferdinand.Core.Features.Security
{
    public class UserInfoByTokenQuery : IRequest<UserInfoByTokenModel>
    {
        public string Token { get; set; }
    }
}
