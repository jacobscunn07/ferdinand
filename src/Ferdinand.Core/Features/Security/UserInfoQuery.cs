﻿using MediatR;

namespace Ferdinand.Core.Features.Security
{
    public class UserInfoQuery : IRequest<UserInfoModel>
    {
        public string EmailAddress { get; set; }
    }
}
