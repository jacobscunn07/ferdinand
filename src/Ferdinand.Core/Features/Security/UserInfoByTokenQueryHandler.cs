﻿using System.Linq;
using Ferdinand.Core.DataAccess;
using MediatR;

namespace Ferdinand.Core.Features.Security
{
    public class UserInfoByTokenQueryHandler : IRequestHandler<UserInfoByTokenQuery, UserInfoByTokenModel>
    {
        private readonly FerdinandContext _ctx;

        public UserInfoByTokenQueryHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public UserInfoByTokenModel Handle(UserInfoByTokenQuery message)
        {
            var person = _ctx.People.SingleOrDefault(x => x.Token.ToString().Equals(message.Token));

            return new UserInfoByTokenModel
            {
                AccessLevel = person.AccessLevel,
                EmailAddress = person.Email,
                FirstName = person.FirstName,
                LastName = person.LastName,
                Id = person.Id
            };
        }
    }
}
