﻿using Ferdinand.Core.Enumerations;
using System;

namespace Ferdinand.Core.Features.Security
{
    public class UserInfoModel
    {
        public Guid Id { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public AccessLevel AccessLevel { get; set; }
        public Guid Token { get; set; }
        public string FullName
        {
            get
            {
                return String.Format("{0}{1}{2}", FirstName,
                    (String.IsNullOrWhiteSpace(FirstName) || String.IsNullOrWhiteSpace(LastName)) ? String.Empty : " ",
                    LastName);
            }
        }
    }
}
