﻿using System;
using MediatR;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Entities;
using Ferdinand.Core.Security;
using Ferdinand.Core.Enumerations;

namespace Ferdinand.Core.Features.Account
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, Guid>
    {
        private readonly FerdinandContext _ctx;
        private readonly ISalt _salt;
        private readonly IEncrypt _encrypt;

        public RegisterCommandHandler(FerdinandContext ctx, ISalt salt, IEncrypt encrypt)
        {
            _ctx = ctx;
            _salt = salt;
            _encrypt = encrypt;
        }

        public Guid Handle(RegisterCommand command)
        {
            var salt = _salt.NewSalt();
            var created = DateTime.Now;
            var person = new Person
            {
                Email = command.Email,
                FirstName = command.FirstName,
                LastName = command.LastName,
                PhoneNumber = command.PhoneNumber,
                PasswordSalt = salt,
                PasswordHash = _encrypt.Encrypt(command.Password, salt),
                CreatedDateTime = created,
                ModifiedDateTime = created,
                AccessLevel = AccessLevel.Admin
            };

            _ctx.People.Add(person);
            _ctx.SaveChanges();

            return person.Id;
        }
    }
}