﻿using MediatR;

namespace Ferdinand.Core.Features.Account
{
    public class LoginCommand : IRequest<LoginCommandModel>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}