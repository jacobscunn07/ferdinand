﻿using MediatR;

namespace Ferdinand.Core.Features.Account
{
    public class LogoffCommand : IRequest<LogoffCommandModel>
    {
    }
}