﻿using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Security;
using MediatR;
using System.Linq;

namespace Ferdinand.Core.Features.Account
{
    public class LoginCommandHandler : IRequestHandler<LoginCommand, LoginCommandModel>
    {
        private readonly FerdinandContext _ctx;
        private readonly IAuthentication _authenticate;
        private readonly IEncrypt _encrypt;

        public LoginCommandHandler(FerdinandContext ctx, IAuthentication authenticate, IEncrypt encrypt)
        {
            _ctx = ctx;
            _authenticate = authenticate;
            _encrypt = encrypt;
        }

        public LoginCommandModel Handle(LoginCommand message)
        {
            var user = _ctx.People.Single(x => x.Email.Equals(message.Email));
            var passwordHash = _encrypt.Encrypt(message.Password, user.PasswordSalt);

            if(user.PasswordHash.Equals(passwordHash))
                _authenticate.Login(message.Email);

            return new LoginCommandModel();
        }
    }
}
