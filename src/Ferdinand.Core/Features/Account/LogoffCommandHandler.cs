﻿using MediatR;
using Ferdinand.Core.Security;

namespace Ferdinand.Core.Features.Account
{
    public class LogoffCommandHandler : IRequestHandler<LogoffCommand, LogoffCommandModel>
    {
        private readonly IAuthentication _authentication;

        public LogoffCommandHandler(IAuthentication authentication)
        {
            _authentication = authentication;
        }

        public LogoffCommandModel Handle(LogoffCommand message)
        {
            _authentication.Logout();
            return new LogoffCommandModel();
        }
    }
}