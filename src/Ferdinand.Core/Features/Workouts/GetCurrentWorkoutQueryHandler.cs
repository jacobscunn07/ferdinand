﻿using System.Data.Entity;
using System.Linq;
using AutoMapper.QueryableExtensions;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Dtos;
using MediatR;

namespace Ferdinand.Core.Features.Workouts
{
    public class GetCurrentWorkoutQueryHandler : IRequestHandler<GetCurrentWorkoutQuery, GetCurrentWorkoutModel>
    {
        private readonly FerdinandContext _ctx;

        public GetCurrentWorkoutQueryHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public GetCurrentWorkoutModel Handle(GetCurrentWorkoutQuery message)
        {
            var workout =
                _ctx.Workouts
                    .Include(
                        x => x.WorkoutSupersets.Select(
                            ws => ws.WorkoutSupersetExercises.Select(
                                    wse =>
                                        wse.WorkoutSupersetExerciseSets.Select(
                                            wses => wses.WorkoutSupersetExerciseSetAttributes.Select(
                                                wsesa => wsesa.Attribute))))).ProjectTo<WorkoutDto>()
                    .First(x => x.PersonId == message.PersonId);

            return new GetCurrentWorkoutModel { Workout = workout ?? new WorkoutDto() };
        }
    }
}
