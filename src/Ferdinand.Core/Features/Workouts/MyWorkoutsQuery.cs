﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts
{
    public class MyWorkoutsQuery : IRequest<MyWorkoutsModel>
    {
        public Guid UserId { get; set; }
    }
}
