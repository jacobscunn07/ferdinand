﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts
{
    public class GetCurrentWorkoutQuery : IRequest<GetCurrentWorkoutModel>
    {
        public Guid PersonId { get; set; }
    }
}
