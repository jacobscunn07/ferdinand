﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Ferdinand.Core.DataAccess;
using MediatR;

namespace Ferdinand.Core.Features.Workouts
{
    public class MyWorkoutsQueryHandler : IRequestHandler<MyWorkoutsQuery, MyWorkoutsModel>
    {
        private readonly FerdinandContext _ctx;

        public MyWorkoutsQueryHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public MyWorkoutsModel Handle(MyWorkoutsQuery message)
        {
            var workouts = _ctx.Workouts
                .Where(x => x.PersonId.Equals(message.UserId))
                .Include(x => x.WorkoutSupersets.Select(y => y.WorkoutSupersetExercises))
                .OrderBy(x => x.CreatedDateTime)
                .ToList();
            var model = new MyWorkoutsModel
            {
                Workouts = Mapper.Map<List<Entities.Workout>, List<MyWorkoutsModel.Workout>>(workouts)
            };

            return model;
        }
    }
}
