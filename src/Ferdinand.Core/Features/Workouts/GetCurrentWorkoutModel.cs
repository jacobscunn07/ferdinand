﻿using Ferdinand.Core.Dtos;

namespace Ferdinand.Core.Features.Workouts
{
    public class GetCurrentWorkoutModel
    {
        public WorkoutDto Workout { get; set; }
    }
}
