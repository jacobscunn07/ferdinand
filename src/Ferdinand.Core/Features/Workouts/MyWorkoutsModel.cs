﻿using System.Collections.Generic;

namespace Ferdinand.Core.Features.Workouts
{
    public class MyWorkoutsModel
    {
        public List<Workout> Workouts { get; set; }

        public MyWorkoutsModel()
        {
            Workouts = new List<Workout>();
        }

        public class Workout
        {
            public Workout()
            {
                WorkoutSupersets = new List<WorkoutSuperset>();
            }

            public string Name { get; set; }
            public string Description { get; set; }
            public List<WorkoutSuperset> WorkoutSupersets { get; set; }
        }

        public class WorkoutSuperset
        {
            public WorkoutSuperset()
            {
                WorkoutSupersetExercises = new List<WorkoutSupersetExercise>();
            }

            public List<WorkoutSupersetExercise> WorkoutSupersetExercises { get; set; }
        }

        public class WorkoutSupersetExercise
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public int Reps { get; set; }
            public bool IsCustomExercise { get; set; }
        }
    }
}
