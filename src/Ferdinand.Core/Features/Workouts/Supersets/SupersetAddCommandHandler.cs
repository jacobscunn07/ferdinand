﻿using System;
using AutoMapper;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Dtos;
using Ferdinand.Core.Entities;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets
{
    public class SupersetAddCommandHandler : IRequestHandler<SupersetAddCommand, SupersetAddModel>
    {
        private readonly FerdinandContext _ctx;

        public SupersetAddCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public SupersetAddModel Handle(SupersetAddCommand command)
        {
            var created = DateTime.Now;
            using (var tx = _ctx.Database.BeginTransaction())
            {
                var superset = new WorkoutSuperset()
                {
                    WorkoutId = command.WorkoutId,
                    CreatedDateTime = created,
                    ModifiedDateTime = created
                };
                _ctx.WorkoutSupersets.Add(superset);
                _ctx.SaveChanges();
                var model = new SupersetAddModel
                {
                    WorkoutSuperset = Mapper.Map<WorkoutSuperset, WorkoutSupersetDto>(superset)
                };
                tx.Commit();

                return model;
            }
        }
    }
}