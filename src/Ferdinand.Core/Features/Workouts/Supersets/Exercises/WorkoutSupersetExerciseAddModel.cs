using Ferdinand.Core.Dtos;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseAddModel
    {
        public WorkoutSupersetExerciseDto WorkoutSupersetExercise { get; set; }
    }
}