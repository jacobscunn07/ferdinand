﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseDeleteCommand : IRequest<bool>
    {
        public Guid WorkoutSupersetExerciseId { get; set; }
    }
}
