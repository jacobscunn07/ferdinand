﻿using System;
using AutoMapper;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Dtos;
using Ferdinand.Core.Entities;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseAddCommandHandler : IRequestHandler<WorkoutSupersetExerciseAddCommand, WorkoutSupersetExerciseAddModel>
    {
        private readonly FerdinandContext _ctx;

        public WorkoutSupersetExerciseAddCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public WorkoutSupersetExerciseAddModel Handle(WorkoutSupersetExerciseAddCommand command)
        {
            using (var tx = _ctx.Database.BeginTransaction())
            {
                var created = DateTime.Now;
                var exercise = new WorkoutSupersetExercise()
                {
                    WorkoutSupersetId = command.WorkoutSupersetId,
                    Name = "New Exercise",
                    Description = "New Exercise",
                    IsCustomExercise = true,
                    CreatedDateTime = created,
                    ModifiedDateTime = created
                };

                _ctx.WorkoutSupersetExercises.Add(exercise);
                _ctx.SaveChanges();
                var model = new WorkoutSupersetExerciseAddModel
                {
                    WorkoutSupersetExercise = Mapper.Map<WorkoutSupersetExercise, WorkoutSupersetExerciseDto>(exercise)
                };
                tx.Commit();
                return model;
            }
        }
    }
}