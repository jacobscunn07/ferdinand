﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseAddCommand : IRequest<WorkoutSupersetExerciseAddModel>
    {
        public Guid WorkoutSupersetId { get; set; }
    }
}
