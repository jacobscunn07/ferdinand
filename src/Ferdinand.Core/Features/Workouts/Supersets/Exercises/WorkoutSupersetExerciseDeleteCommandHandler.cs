using System.Data.Entity;
using System.Linq;
using Ferdinand.Core.DataAccess;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseDeleteCommandHandler : IRequestHandler<WorkoutSupersetExerciseDeleteCommand, bool>
    {
        private readonly FerdinandContext _ctx;

        public WorkoutSupersetExerciseDeleteCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public bool Handle(WorkoutSupersetExerciseDeleteCommand command)
        {
            using (var tx = _ctx.Database.BeginTransaction())
            {
                var exercise = _ctx.WorkoutSupersetExercises
                    .Include(
                        wse =>
                            wse.WorkoutSupersetExerciseSets
                                .Select(wses => wses.WorkoutSupersetExerciseSetAttributes))
                    .Single(x => x.Id == command.WorkoutSupersetExerciseId);

                _ctx.WorkoutSupersetExercises.Remove(exercise);
                _ctx.SaveChanges();
                tx.Commit();
                return true;
            }
        }
    }
}