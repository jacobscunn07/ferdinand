﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseCopyCommand : IRequest<WorkoutSupersetExerciseCopyModel>
    {
        public Guid WorkoutSupersetId { get; set; }
        public Guid WorkoutSupersetExerciseId { get; set; }
    }
}
