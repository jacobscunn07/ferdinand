using Ferdinand.Core.Dtos;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseCopyModel
    {
        public WorkoutSupersetExerciseDto WorkoutSupersetExercise { get; set; }
    }
}