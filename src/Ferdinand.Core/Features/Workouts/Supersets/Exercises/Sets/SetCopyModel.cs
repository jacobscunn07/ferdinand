using Ferdinand.Core.Dtos;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetCopyModel
    {
        public WorkoutSupersetExerciseSetDto WorkoutSupersetExerciseSet { get; set; }
    }
}