﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetDeleteCommand : IRequest<bool>
    {
        public Guid WorkoutSupersetExerciseSetId { get; set; }
    }
}
