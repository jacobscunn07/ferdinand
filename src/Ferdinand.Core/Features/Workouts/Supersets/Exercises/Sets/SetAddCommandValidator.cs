using System;
using System.Linq;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Security;
using FluentValidation;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetAddCommandValidator : AbstractValidator<SetAddCommand>
    {
        private readonly IUserSession _userSession;
        private readonly FerdinandContext _ctx;

        public SetAddCommandValidator(IUserSession userSession, FerdinandContext ctx)
        {
            _userSession = userSession;
            _ctx = ctx;

            RuleFor(x => x.WorkoutSupersetExerciseId)
                .NotEmpty()
                .WithMessage("Exercise Id is required.")
                .Must(UserCanAccessExercise)
                .WithMessage("User does not have access to this exercise.");
        }

        private bool UserCanAccessExercise(Guid exerciseId)
        {
            var user = _userSession.GetCurrentUser();
            return
                _ctx.People.Any(
                    p =>
                        p.Id == user.Id &&
                        p.Workouts.Any(
                            w =>
                                w.WorkoutSupersets.Any(
                                    ws => ws.WorkoutSupersetExercises.Any(wse => wse.Id == exerciseId))));
        }
    }
}