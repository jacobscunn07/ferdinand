﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Dtos;
using Ferdinand.Core.Entities;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetCopyCommandHandler : IRequestHandler<SetCopyCommand, SetCopyModel>
    {
        private readonly FerdinandContext _ctx;

        public SetCopyCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public SetCopyModel Handle(SetCopyCommand command)
        {
            using (var tx = _ctx.Database.BeginTransaction())
            {
                var created = DateTime.Now;
                var setToCopy =
                    _ctx.WorkoutSupersetExerciseSets
                        .Include(x => x.WorkoutSupersetExerciseSetAttributes
                            .Select(y => y.Attribute))
                        .Single(x => x.Id == command.WorkoutSupersetExerciseSetId);

                var attributes = new List<WorkoutSupersetExerciseSetAttribute>();

                foreach (var attr in setToCopy.WorkoutSupersetExerciseSetAttributes)
                {
                    attributes.Add(new WorkoutSupersetExerciseSetAttribute
                    {
                        AttributeId = attr.AttributeId,
                        CreatedDateTime = created,
                        ModifiedDateTime = created,
                        Value = attr.Value
                    });
                }

                var set = new WorkoutSupersetExerciseSet
                {
                    CreatedDateTime = created,
                    ModifiedDateTime = created,
                    WorkoutSupersetExerciseId = command.WorkoutSupersetExerciseId,
                    WorkoutSupersetExerciseSetAttributes = attributes
                };
                _ctx.WorkoutSupersetExerciseSets.Add(set);
                _ctx.SaveChanges();
                var model = new SetCopyModel
                {
                    WorkoutSupersetExerciseSet =
                        Mapper.Map<WorkoutSupersetExerciseSet, WorkoutSupersetExerciseSetDto>(set)
                };
                tx.Commit();
                return model;
            }
        }
    }
}