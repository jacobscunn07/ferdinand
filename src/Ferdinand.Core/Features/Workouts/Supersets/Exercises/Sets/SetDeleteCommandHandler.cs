﻿using System.Data.Entity;
using System.Linq;
using Ferdinand.Core.DataAccess;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetDeleteCommandHandler : IRequestHandler<SetDeleteCommand, bool>
    {
        private readonly FerdinandContext _ctx;

        public SetDeleteCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public bool Handle(SetDeleteCommand command)
        {
            using (var tx = _ctx.Database.BeginTransaction())
            {
                var set =
                    _ctx.WorkoutSupersetExerciseSets.Include(x => x.WorkoutSupersetExerciseSetAttributes)
                        .Single(x => x.Id == command.WorkoutSupersetExerciseSetId);
                _ctx.WorkoutSupersetExerciseSets.Remove(set);
                _ctx.SaveChanges();
                tx.Commit();
                return true;
            }
        }
    }
}