﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetAddCommand : IRequest<SetAddModel>
    {
        public Guid WorkoutSupersetExerciseId { get; set; }
    }
}
