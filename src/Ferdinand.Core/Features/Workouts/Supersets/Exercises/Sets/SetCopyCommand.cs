﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetCopyCommand : IRequest<SetCopyModel>
    {
        public Guid WorkoutSupersetExerciseId { get; set; }
        public Guid WorkoutSupersetExerciseSetId { get; set; }
    }
}
