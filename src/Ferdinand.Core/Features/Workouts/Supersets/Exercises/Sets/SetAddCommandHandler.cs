﻿using System;
using AutoMapper;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Dtos;
using Ferdinand.Core.Entities;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetAddCommandHandler : IRequestHandler<SetAddCommand, SetAddModel>
    {
        private readonly FerdinandContext _ctx;

        public SetAddCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public SetAddModel Handle(SetAddCommand command)
        {
            var created = DateTime.Now;
            using (var tx = _ctx.Database.BeginTransaction())
            {
                var set = new WorkoutSupersetExerciseSet()
                {
                    WorkoutSupersetExerciseId = command.WorkoutSupersetExerciseId,
                    CreatedDateTime = created,
                    ModifiedDateTime = created
                };
                _ctx.WorkoutSupersetExerciseSets.Add(set);
                _ctx.SaveChanges();
                var model = new SetAddModel
                {
                    WorkoutSupersetExerciseSet = Mapper.Map<WorkoutSupersetExerciseSet, WorkoutSupersetExerciseSetDto>(set)
                };
                tx.Commit();

                return model;
            }
        }
    }
}