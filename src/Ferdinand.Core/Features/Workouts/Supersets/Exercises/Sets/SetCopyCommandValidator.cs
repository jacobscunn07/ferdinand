using System;
using FluentValidation;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets
{
    public class SetCopyCommandValidator : AbstractValidator<SetCopyCommand>
    {
        public SetCopyCommandValidator()
        {
            RuleFor(x => x.WorkoutSupersetExerciseId)
                .NotEmpty()
                .WithMessage("Exercise Id is required");
        }

        private bool UserCanAccessExercise()
        {
            throw new NotImplementedException();
        }
    }
}