using System;
using FluentValidation;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseCopyCommandValidator : AbstractValidator<WorkoutSupersetExerciseCopyCommand>
    {
        public WorkoutSupersetExerciseCopyCommandValidator()
        {
            RuleFor(x => x.WorkoutSupersetExerciseId)
                .NotEmpty()
                .WithMessage("Exercise Id is required");
        }

        private bool UserCanAccessExercise()
        {
            throw new NotImplementedException();
        }
    }
}