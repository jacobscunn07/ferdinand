using System;
using System.Linq;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Security;
using FluentValidation;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseAddCommandValidator : AbstractValidator<WorkoutSupersetExerciseAddCommand>
    {
        private readonly IUserSession _userSession;
        private readonly FerdinandContext _ctx;

        public WorkoutSupersetExerciseAddCommandValidator(IUserSession userSession, FerdinandContext ctx)
        {
            _userSession = userSession;
            _ctx = ctx;

            RuleFor(x => x.WorkoutSupersetId)
                .NotEmpty()
                .WithMessage("Superset Id is required.")
                .Must(UserCanAccessExercise)
                .WithMessage("User does not have access to this superset.");
        }

        private bool UserCanAccessExercise(Guid supersetId)
        {
            var user = _userSession.GetCurrentUser();
            return
                _ctx.People.Any(
                    p =>
                        p.Id == user.Id &&
                        p.Workouts.Any(
                            w =>
                                w.WorkoutSupersets.Any(
                                    ws => ws.Id == supersetId)));
        }
    }
}