﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Dtos;
using Ferdinand.Core.Entities;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets.Exercises
{
    public class WorkoutSupersetExerciseCopyCommandHandler : IRequestHandler<WorkoutSupersetExerciseCopyCommand, WorkoutSupersetExerciseCopyModel>
    {
        private readonly FerdinandContext _ctx;

        public WorkoutSupersetExerciseCopyCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public WorkoutSupersetExerciseCopyModel Handle(WorkoutSupersetExerciseCopyCommand command)
        {
            using (var tx = _ctx.Database.BeginTransaction())
            {
                var created = DateTime.Now;
                var exerciseToCopy =
                    _ctx.WorkoutSupersetExercises
                        .Include(x => x.WorkoutSupersetExerciseSets
                            .Select(y => y.WorkoutSupersetExerciseSetAttributes
                                .Select(z => z.Attribute)))
                        .Single(x => x.Id == command.WorkoutSupersetExerciseId);

                var exerciseSets =
                    exerciseToCopy.WorkoutSupersetExerciseSets.Select(set => new WorkoutSupersetExerciseSet
                    {
                        CreatedDateTime = created,
                        ModifiedDateTime = created,
                        WorkoutSupersetExerciseSetAttributes =
                            set.WorkoutSupersetExerciseSetAttributes.Select(
                                x => new WorkoutSupersetExerciseSetAttribute()
                                {
                                    AttributeId = x.AttributeId,
                                    CreatedDateTime = created,
                                    ModifiedDateTime = created,
                                    Value = x.Value
                                }).ToList()
                    }).ToList();

                var exercise = new WorkoutSupersetExercise
                {
                    Name = "Copied Exercise",
                    Description = "Copied Exercise",
                    IsCustomExercise = true,
                    CreatedDateTime = created,
                    ModifiedDateTime = created,
                    WorkoutSupersetId = command.WorkoutSupersetId,
                    WorkoutSupersetExerciseSets = exerciseSets
                };

                _ctx.WorkoutSupersetExercises.Add(exercise);
                _ctx.SaveChanges();
                var model = new WorkoutSupersetExerciseCopyModel
                {
                    WorkoutSupersetExercise = Mapper.Map<WorkoutSupersetExercise, WorkoutSupersetExerciseDto>(exercise)
                };
                tx.Commit();
                return model;
            }
        }
    }
}