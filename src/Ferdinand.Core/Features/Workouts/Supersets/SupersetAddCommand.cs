﻿using System;
using MediatR;

namespace Ferdinand.Core.Features.Workouts.Supersets
{
    public class SupersetAddCommand : IRequest<SupersetAddModel>
    {
        public Guid WorkoutId { get; set; }
    }
}
