using Ferdinand.Core.Dtos;

namespace Ferdinand.Core.Features.Workouts.Supersets
{
    public class SupersetAddModel
    {
        public WorkoutSupersetDto WorkoutSuperset { get; set; }
    }
}