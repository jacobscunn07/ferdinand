using System;
using System.Linq;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Security;
using FluentValidation;

namespace Ferdinand.Core.Features.Workouts.Supersets
{
    public class SupersetAddCommandValidator : AbstractValidator<SupersetAddCommand>
    {
        private readonly IUserSession _userSession;
        private readonly FerdinandContext _ctx;

        public SupersetAddCommandValidator(IUserSession userSession, FerdinandContext ctx)
        {
            _userSession = userSession;
            _ctx = ctx;

            RuleFor(x => x.WorkoutId)
                .NotEmpty()
                .WithMessage("Workout Id is required.")
                .Must(UserCanAccessWorkout)
                .WithMessage("User does not have access to this workout.");
        }

        private bool UserCanAccessWorkout(Guid workoutId)
        {
            var user = _userSession.GetCurrentUser();
            return
                _ctx.People.Any(p =>
                    p.Id == user.Id && p.Workouts.Any(w =>
                        w.Id == workoutId));
        }
    }
}