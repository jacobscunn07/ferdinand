﻿using MediatR;
using System.Collections.Generic;

namespace Ferdinand.Core.Features.MyWorkoutTemplates
{
    public class AddTemplateCommand : IRequest<AddTemplateCommandModel>
    {
        public class Tempate
        {
            List<TemplateSuperset> TemplateSupersets = new List<TemplateSuperset>();
            public string Name { get; set; }
            public string Description { get; set; }
        }

        public class TemplateSuperset
        {
            List<TemplateSupersetExercise> TemplateSupersetExercises = new List<TemplateSupersetExercise>();
            public int RestPeriod { get; set; }
        }

        public class TemplateSupersetExercise
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public int Reps { get; set; }
            public bool IsCustomExercise { get; set; }
        }
    }
}
