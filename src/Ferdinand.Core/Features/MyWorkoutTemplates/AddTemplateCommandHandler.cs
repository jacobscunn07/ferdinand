﻿using MediatR;
using Ferdinand.Core.DataAccess;
using System;

namespace Ferdinand.Core.Features.MyWorkoutTemplates
{
    public class AddTemplateCommandHandler : IRequestHandler<AddTemplateCommand, AddTemplateCommandModel>
    {
        private readonly FerdinandContext _ctx;

        public AddTemplateCommandHandler(FerdinandContext ctx)
        {
            _ctx = ctx;
        }

        public AddTemplateCommandModel Handle(AddTemplateCommand message)
        {
            throw new NotImplementedException();
        }
    }
}
