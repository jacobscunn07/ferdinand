﻿using System;
using System.Collections.Generic;

namespace Ferdinand.Core.Entities
{
    public class TemplateSuperset : BaseEntity
    {
        public Guid TemplateId { get; set; }
        public Template Template { get; set; }
        public ICollection<TemplateSupersetExercise> TemplateSupersetExercises { get; set; }
    }
}