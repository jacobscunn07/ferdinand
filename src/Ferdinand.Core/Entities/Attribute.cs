﻿using System.Collections.Generic;

namespace Ferdinand.Core.Entities
{
    public class Attribute : BaseEntity
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public ICollection<WorkoutSupersetExerciseSetAttribute> WorkoutSupersetExerciseSetAttributes { get; set; }
        public ICollection<ExerciseAttribute> ExerciseAttributes { get; set; }
    }
}
