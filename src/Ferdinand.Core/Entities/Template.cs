﻿using System;
using System.Collections.Generic;

namespace Ferdinand.Core.Entities
{
    public class Template : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid PersonId { get; set; }
        public Person Person { get; set; }
        public ICollection<TemplateSuperset> TemplateSupersets { get; set; }
    }
}