﻿using Ferdinand.Core.Enumerations;
using System.Collections.Generic;

namespace Ferdinand.Core.Entities
{
    public class ExerciseCategory : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Exercise> Exercises { get; set; }
    }
}