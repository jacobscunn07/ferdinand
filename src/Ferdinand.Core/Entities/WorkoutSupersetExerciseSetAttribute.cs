﻿using System;

namespace Ferdinand.Core.Entities
{
    public class WorkoutSupersetExerciseSetAttribute : BaseEntity
    {
        public Guid WorkoutSupersetExerciseSetId { get; set; }
        public WorkoutSupersetExerciseSet WorkoutSupersetExerciseSet { get; set; }
        public Guid AttributeId { get; set; }
        public Attribute Attribute { get; set; }
        public string Value { get; set; }
    }
}
