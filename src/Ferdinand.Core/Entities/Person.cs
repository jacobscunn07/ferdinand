﻿using System;
using System.Collections;
using System.Collections.Generic;
using Ferdinand.Core.Enumerations;

namespace Ferdinand.Core.Entities
{
    public class Person : BaseEntity
    {
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public AccessLevel AccessLevel { get; set; }
        public int AccessLevelId
        {
            get { return AccessLevel.Value; }
            set { AccessLevel = AccessLevel.FromValue(value); }
        }
        public Guid Token { get; set; }

        public ICollection<Template> Templates { get; set; }
        public ICollection<Workout> Workouts { get; set; }
    }
}