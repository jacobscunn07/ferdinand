﻿using Ferdinand.Core.Enumerations;
using System.Collections.Generic;

namespace Ferdinand.Core.Entities
{
    public class Exercise : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<ExerciseCategory> ExerciseCategories { get; set; }
    }
}