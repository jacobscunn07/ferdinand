﻿using System;

namespace Ferdinand.Core.Entities
{
    public class ExerciseAttribute : BaseEntity
    {
        public string Value { get; set; }
        public Guid ExerciseId { get; set; }
        public Exercise Exercise { get; set; }
        public Guid AttributeId { get; set; }
        public Attribute Attribute { get; set; }
    }
}
