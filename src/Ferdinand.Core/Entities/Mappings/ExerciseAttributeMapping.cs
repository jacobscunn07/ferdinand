﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ferdinand.Core.Entities.Mappings
{
    public class ExerciseAttributeMapping : EntityTypeConfiguration<ExerciseAttribute>
    {
        public ExerciseAttributeMapping()
        {
            HasKey(x => x.Id);
            ToTable("ExerciseAttribute");
            Property(x => x.Value).HasMaxLength(256);
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.ExerciseId).IsRequired();
            Property(x => x.AttributeId).IsRequired();
        }
    }
}
