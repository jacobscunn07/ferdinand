﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class WorkoutSupersetExerciseSetAttributeMapping : EntityTypeConfiguration<WorkoutSupersetExerciseSetAttribute>
    {
        public WorkoutSupersetExerciseSetAttributeMapping()
        {
            HasKey(x => x.Id);
            ToTable("WorkoutSupersetExerciseSetAttribute");
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.AttributeId).IsRequired();
            Property(x => x.WorkoutSupersetExerciseSetId).IsRequired();
            Property(x => x.Value);
        }
    }
}
