﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class WorkoutMapping : EntityTypeConfiguration<Workout>
    {
        public WorkoutMapping()
        {
            HasKey(x => x.Id);
            ToTable("Workout");
            Property(x => x.Description).IsMaxLength();
            Property(x => x.Name).HasMaxLength(256);
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.PersonId).IsRequired();
            HasMany(x => x.WorkoutSupersets);
        }
    }
}