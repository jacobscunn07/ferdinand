﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class PersonMapping : EntityTypeConfiguration<Person>
    {
        public PersonMapping()
        {
            HasKey(x => x.Id);
            ToTable("Person");
            Ignore(x => x.AccessLevel);
            Property(x => x.Email).HasMaxLength(256);
            Property(x => x.EmailConfirmed).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.PasswordHash).IsMaxLength().IsRequired();
            Property(x => x.PasswordSalt).IsMaxLength();
            Property(x => x.PhoneNumber).IsMaxLength();
            Property(x => x.PhoneNumberConfirmed).IsRequired();
            Property(x => x.FirstName);
            Property(x => x.LastName);
            Property(x => x.AccessLevelId).IsRequired();
            Property(x => x.Token).IsRequired();
            HasMany(x => x.Templates);
            HasMany(x => x.Workouts);
        }
    }
}