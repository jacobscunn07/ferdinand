﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class AttributeMapping : EntityTypeConfiguration<Attribute>
    {
        public AttributeMapping()
        {
            HasKey(x => x.Id);
            ToTable("Attribute");
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.DataType).IsRequired();
            Property(x => x.Name);
            HasMany(x => x.WorkoutSupersetExerciseSetAttributes);
            HasMany(x => x.ExerciseAttributes);
        }
    }
}
