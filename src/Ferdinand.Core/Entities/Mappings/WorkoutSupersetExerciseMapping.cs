﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class WorkoutSupersetExerciseMapping : EntityTypeConfiguration<WorkoutSupersetExercise>
    {
        public WorkoutSupersetExerciseMapping()
        {
            HasKey(x => x.Id);
            ToTable("WorkoutSupersetExercise");
            Property(x => x.Description).IsMaxLength();
            Property(x => x.IsCustomExercise).IsRequired();
            Property(x => x.Name).IsRequired().HasMaxLength(256);
            Property(x => x.WorkoutSupersetId).IsRequired();
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.WorkoutSupersetId).IsRequired();
            Property(x=> x.ExerciseId).IsOptional();
            HasMany(x => x.WorkoutSupersetExerciseSets)
                .WithRequired(x => x.WorkoutSupersetExercise)
                .HasForeignKey(x => x.WorkoutSupersetExerciseId)
                .WillCascadeOnDelete();
        }
    }
}