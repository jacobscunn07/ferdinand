﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class TemplateMapping : EntityTypeConfiguration<Template>
    {
        public TemplateMapping()
        {
            HasKey(x => x.Id);
            ToTable("Template");
            Property(x => x.Name).HasMaxLength(256).IsRequired();
            Property(x => x.Description).IsMaxLength();
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.PersonId).IsRequired();
            HasMany(x => x.TemplateSupersets);
        }
    }
}
