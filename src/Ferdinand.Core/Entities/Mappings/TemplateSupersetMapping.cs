﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class TemplateSupersetMapping : EntityTypeConfiguration<TemplateSuperset>
    {
        public TemplateSupersetMapping()
        {
            HasKey(x => x.Id);
            ToTable("TemplateSuperset");
            Property(x => x.TemplateId).IsRequired();
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            HasMany(x => x.TemplateSupersetExercises);
        }
    }
}