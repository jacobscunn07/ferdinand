﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class ExerciseMapping : EntityTypeConfiguration<Exercise>
    {
        public ExerciseMapping()
        {
            HasKey(x => x.Id);
            ToTable("Exercise");
            Property(x => x.Name).IsRequired().HasMaxLength(256);
            Property(x => x.Description).IsRequired().IsMaxLength();
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            HasMany(x => x.ExerciseCategories)
                .WithMany(x => x.Exercises)
                .Map(x =>
                {
                    x.ToTable("ExerciseCategoryExercise");
                    x.MapLeftKey("ExerciseId");
                    x.MapRightKey("ExerciseCategoryId");
                });
        }
    }
}
