﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class ExerciseCategoryMapping : EntityTypeConfiguration<ExerciseCategory>
    {
        public ExerciseCategoryMapping()
        {
            HasKey(x => x.Id);
            ToTable("ExerciseCategory");
            Property(x => x.Name).IsRequired().HasMaxLength(256);
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            HasMany(x => x.Exercises)
                .WithMany(x => x.ExerciseCategories)
                .Map(x =>
                {
                    x.ToTable("ExerciseCategoryExercise");
                    x.MapLeftKey("ExerciseCategoryId");
                    x.MapRightKey("ExerciseId");
                });
        }
    }
}
