﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class WorkoutSupersetExerciseSetMapping : EntityTypeConfiguration<WorkoutSupersetExerciseSet>
    {
        public WorkoutSupersetExerciseSetMapping()
        {
            HasKey(x => x.Id);
            ToTable("WorkoutSupersetExerciseSet");
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.WorkoutSupersetExerciseId).IsRequired();
            HasMany(x => x.WorkoutSupersetExerciseSetAttributes);
        }
    }
}
