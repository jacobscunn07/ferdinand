﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class TemplateSupersetExerciseMapping : EntityTypeConfiguration<TemplateSupersetExercise>
    {
        public TemplateSupersetExerciseMapping()
        {
            HasKey(x => x.Id);
            ToTable("TemplateSupersetExercise");
            Property(x => x.Description).IsMaxLength();
            Property(x => x.Name).HasMaxLength(256).IsRequired();
            Property(x => x.Reps).IsRequired();
            Property(x => x.IsCustomExercise).IsRequired();
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            Property(x => x.TemplateSupersetId).IsRequired();
        }
    }
}
