﻿using System.Data.Entity.ModelConfiguration;

namespace Ferdinand.Core.Entities.Mappings
{
    public class WorkoutSupersetMapping : EntityTypeConfiguration<WorkoutSuperset>
    {
        public WorkoutSupersetMapping()
        {
            HasKey(x => x.Id);
            ToTable("WorkoutSuperset");
            Property(x => x.WorkoutId).IsRequired();
            Property(x => x.CreatedDateTime).IsRequired();
            Property(x => x.ModifiedDateTime).IsRequired();
            HasMany(x => x.WorkoutSupersetExercises);
        }
    }
}