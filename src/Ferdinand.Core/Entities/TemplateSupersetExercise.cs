﻿using Ferdinand.Core.Enumerations;
using System;

namespace Ferdinand.Core.Entities
{
    public class TemplateSupersetExercise : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Reps { get; set; }
        public bool IsCustomExercise { get; set; }
        public Guid TemplateSupersetId { get; set; }
        public TemplateSuperset TemplateSuperset { get; set; }
        
    }
}