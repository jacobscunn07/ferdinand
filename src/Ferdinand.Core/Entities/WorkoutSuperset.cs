﻿using System;
using System.Collections.Generic;

namespace Ferdinand.Core.Entities
{
    public class WorkoutSuperset : BaseEntity
    {
        public Guid WorkoutId { get; set; }
        public Workout Workout { get; set; }
        public ICollection<WorkoutSupersetExercise> WorkoutSupersetExercises { get; set; }
    }
}