﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ferdinand.Core.Entities
{
    public class WorkoutSupersetExerciseSet : BaseEntity
    {
        public Guid WorkoutSupersetExerciseId { get; set; }
        public virtual WorkoutSupersetExercise WorkoutSupersetExercise { get; set; }
        public virtual ICollection<WorkoutSupersetExerciseSetAttribute> WorkoutSupersetExerciseSetAttributes { get; set; }
    }
}
