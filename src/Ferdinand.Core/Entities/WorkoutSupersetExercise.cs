﻿using Ferdinand.Core.Enumerations;
using System;
using System.Collections.Generic;

namespace Ferdinand.Core.Entities
{
    public class WorkoutSupersetExercise : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsCustomExercise { get; set; }
        public Guid WorkoutSupersetId { get; set; }
        public virtual WorkoutSuperset WorkoutSuperset { get; set; }
        public Guid? ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }
        public virtual ICollection<WorkoutSupersetExerciseSet> WorkoutSupersetExerciseSets { get; set; }
    }
}