﻿using System;
using System.Linq;
using Ferdinand.Core.DataAccess;
using MediatR;
using System.Web.Mvc;
using Ferdinand.Core.Security;
using Ferdinand.Core.Features.Home;
using System.Data.Entity;
using System.IO;
using System.Web;

namespace Ferdinand.UI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;
        private readonly FerdinandContext _ctx;
        private readonly IUserSession _userSession;
        private readonly IUser _user;

        public HomeController(IMediator mediator, IUserSession userSession)
        {
            _mediator = mediator;
            _userSession = userSession;
        }

        public ActionResult Index()
        {
            var user = _userSession.GetCurrentUser();
            return View(user.Token);
        }

        [ChildActionOnly]
        [AllowAnonymous]
        public string _RequireJS(ViewContext context)
        {
            var view = context.View as RazorView;
            var values = context.RouteData.Values;
            var controller = values["controller"].ToString().ToLower();
            var viewName = Path.GetFileNameWithoutExtension(view.ViewPath).ToLower();
            var requirePath = VirtualPathUtility.ToAbsolute("~/Scripts/require.js");
            //var loc = $"~/Scripts/app/{controller}/{viewName}.js";
            var loc = string.Format("~/Scripts/app/{0}/{1}.js", controller, viewName);

            if (System.IO.File.Exists(Server.MapPath(loc)))
            {
                return "<script data-main='" + VirtualPathUtility.ToAbsolute(loc) + "' src='" + requirePath +
                       "'></script>";
            }
            return "<script data-main='" + VirtualPathUtility.ToAbsolute("~/Scripts/app/Shared/Default.js") + "' src='" + requirePath +
                       "'></script>";
        }
        [AllowAnonymous]
        public ActionResult _UserMenu()
        {
            var model = new UserMenuModel();
            if (_userSession.IsAuthenticated())
                model.UserName = _userSession.GetCurrentUser().FullName;
            return View(model);
        }
    }
}