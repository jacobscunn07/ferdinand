﻿using System.Web.Mvc;
using MediatR;
using Ferdinand.Core.Features.Account;

namespace Ferdinand.UI.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginCommand model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(new LoginCommand { Email = model.Email, Password = model.Password });
                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        public ActionResult LogOff()
        {
            _mediator.Send(new LogoffCommand());
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterCommand model)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(model);
                _mediator.Send(new LoginCommand { Password = model.Password, Email = model.Email });

                return RedirectToAction("Index", "Home");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
    }
}