﻿using System.Web.Mvc;
using Ferdinand.Core.Features.Workouts;
using MediatR;
using Ferdinand.Core.Security;

namespace Ferdinand.UI.Controllers
{
    [Authorize]
    public class MyWorkoutsController : Controller
    {
        private readonly IMediator _mediator;
        private IUserSession _userSession;

        public MyWorkoutsController(IMediator mediator, IUserSession userSession)
        {
            _mediator = mediator;
            _userSession = userSession;
        }

        public ActionResult Index()
        {
            var workouts = _mediator.Send(new MyWorkoutsQuery() { UserId = _userSession.GetCurrentUser().Id });
            return View(workouts);
        }

        public ActionResult Current()
        {
            var workout = _mediator.Send(new GetCurrentWorkoutQuery {PersonId = _userSession.GetCurrentUser().Id});
            return View(workout);
        }
    }
}