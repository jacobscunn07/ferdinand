﻿using System.Web.Mvc;

namespace Ferdinand.UI.Controllers
{
    [Authorize]
    public class MyTemplatesController : Controller
    {
        // GET: MyTemplates
        public ActionResult Index()
        {
            return View();
        }
    }
}