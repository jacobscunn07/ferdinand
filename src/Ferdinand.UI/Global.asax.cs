﻿using Ferdinand.UI.Core.Initialization;

namespace Ferdinand.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            FerdinandBootstrapper.Bootstrap();
        }
    }
}
