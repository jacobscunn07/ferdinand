﻿using System.Web;
using Ferdinand.Core.Features.Security;
using Ferdinand.Core.Security;
using MediatR;

namespace Ferdinand.UI.Core.Security
{
    public class UserSession : IUserSession
    {
        private readonly IMediator _mediator;

        public UserSession(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IUser GetCurrentUser()
        {
            var emailAddress = HttpContext.Current.User.Identity.Name;
            var userInfo = _mediator.Send(new UserInfoQuery {EmailAddress = emailAddress});
            return new UserInfo(userInfo.Id, userInfo.EmailAddress, userInfo.AccessLevel)
            {
                 FirstName = userInfo.FirstName,
                 LastName = userInfo.LastName,
                 Token = userInfo.Token
            };
        }

        public bool IsAuthenticated()
        {
            return (HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated;
        }
    }
}