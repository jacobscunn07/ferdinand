﻿using Ferdinand.Core.Security;
using System.Web.Security;

namespace Ferdinand.UI.Core.Security
{
    public class WebFormsAuthentication : IAuthentication
    {
        public void Login(string username, bool rememberMe = false)
        {
            FormsAuthentication.SetAuthCookie(username, rememberMe);
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}