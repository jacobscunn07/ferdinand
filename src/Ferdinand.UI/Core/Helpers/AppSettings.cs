﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Ferdinand.Core.Extensions;

namespace Ferdinand.UI.Core.Helpers
{
    public static class AppSettings
    {

        public static IDictionary<string, string> ClientAppSettings
        {
            get
            {
                var keys = new List<string>() { "WebApiUrl" };
                return ConfigurationManager
                    .AppSettings
                    .AllKeys
                    .Where(x => keys.Contains(x))
                    .ToDictionary(key => key, key => ConfigurationManager.AppSettings[key]);

            }
        }
    }
}