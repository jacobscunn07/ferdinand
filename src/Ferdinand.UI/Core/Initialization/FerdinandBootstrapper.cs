﻿using AutoMapper;
using Ferdinand.Core.AutoMapper;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Ferdinand.UI.Core.Initialization
{
    public static class FerdinandBootstrapper
    {
        public static void Bootstrap()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeAutoMapper();
        }

        public static void InitializeAutoMapper()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<QueryAutoMapperProfile>());
        }
    }
}