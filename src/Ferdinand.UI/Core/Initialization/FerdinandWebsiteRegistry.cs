﻿using System.Web;
using Ferdinand.Core.Security;
using Ferdinand.UI.Core.Security;
using StructureMap.Configuration.DSL;

namespace Ferdinand.UI.Core.Initialization
{
    public class FerdinandWebsiteRegistry : Registry
    {
        public FerdinandWebsiteRegistry()
        {
            For<HttpRequest>().Use(() => HttpContext.Current.Request);
            For<IAuthentication>().Use(() => new WebFormsAuthentication());
            For<IUserSession>().Use<UserSession>();
        }
    }
}