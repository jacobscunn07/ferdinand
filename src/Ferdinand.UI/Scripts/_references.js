/// <autosync enabled="true" />
/// <reference path="app/home/index.js" />
/// <reference path="app/main.js" />
/// <reference path="app/models/exercise.js" />
/// <reference path="app/models/set.js" />
/// <reference path="app/models/superset.js" />
/// <reference path="app/models/workout.js" />
/// <reference path="app/myworkouts/current.js" />
/// <reference path="app/myworkouts/index.js" />
/// <reference path="app/shared/default.js" />
/// <reference path="bootstrap.js" />
/// <reference path="bootstrap4.js" />
/// <reference path="delta/js/animations.js" />
/// <reference path="delta/js/main.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="knockout-3.4.0.js" />
/// <reference path="lodash.core.min.js" />
/// <reference path="lodash.min.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="r.js" />
/// <reference path="require.js" />
/// <reference path="respond.js" />
