﻿require(['/Scripts/app/main.js'], function() {
    require(['ko'], function(ko) {
        window.ko = ko; //manually set the global ko property.
        $(function () {
            require(['modelData'], function (modelData) {
                //build ko vm here...
                var viewModel = function () {
                    var self = this;
                    self.model = ko.observableArray(modelData.model.workouts);
                }

                var vm = new viewModel();
                ko.applyBindings(vm);
            });
        });
    });
});