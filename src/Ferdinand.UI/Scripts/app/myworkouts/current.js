﻿require(['/Scripts/app/main.js'], function () {
    require(['ko'], function (ko) {
        window.ko = ko; //manually set the global ko property.
        $(function () {
            require(['modelData', 'workout'], function (modelData, Workout) {
                var self = this;
                self.workout = new Workout(modelData.model.workout);
                ko.applyBindings();
            });
        });
    });
});