﻿require(['/Scripts/app/main.js'], function() {
    require(['ko'], function(ko) {
        window.ko = ko; //manually set the global ko property.
        $(function () {
            require(['modelData'], function (modelData) {
                //build ko vm here...
                localStorage.setItem("ferdinandApiToken", modelData.token);
            });
        });
    });
});