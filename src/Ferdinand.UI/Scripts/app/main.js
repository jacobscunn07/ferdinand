﻿require.config({
    baseUrl: '/Scripts',
    paths: {
        //Libraries
        'ko': 'knockout-3.4.0',
        '_': 'lodash',

        //Models
        'workout': 'app/models/workout',
        'superset': 'app/models/superset',
        'exercise': 'app/models/exercise',
        'set': 'app/models/set'
    }
});