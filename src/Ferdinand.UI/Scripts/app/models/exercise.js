﻿define(["ko", "set"],
    function (ko, Set) {
        var Exercise = function (model) {
            var self = this;

            self.id = model.id;
            self.name = model.Name;
            self.description = model.Description;
            self.sets = ko.observableArray(_.map(model.supersetExerciseSets, function (data) { return new Set(data) }));

            self.getHtmlPanelId = function () {
                return "exercise-panel_" + self.id;
            }

            self.getHtmlPanelHeadingId = function () {
                return "exercise-panel-heading_" + self.id;
            }

            self.getHtmlPanelBodyId = function () {
                return "exercise-panel-body_" + self.id;
            }

            self.createSet = function () {
                var data = {
                    WorkoutSupersetExerciseId: self.id
                };

                var ajaxOptions = {
                    method: "POST",
                    url: "http://api.ferdinand.com/api/set",
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'X-Ferdy-ApiToken': localStorage.getItem("ferdinandApiToken"),
                        'Content-Type': 'application/json'
                    }
                };

                $.ajax(ajaxOptions)
                    .success(function(data) {
                        self.sets.push(new Set(data.workoutSupersetExerciseSet));
                    })
                    .error(function (data) {
                    });
            }

            self.copySet = function (set) {
                var data = {
                    WorkoutSupersetExerciseId: self.id,
                    WorkoutSupersetExerciseSetId: set.id
                };

                var ajaxOptions = {
                    method: "POST",
                    url: "http://api.ferdinand.com/api/setcopy",
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'X-Ferdy-ApiToken': localStorage.getItem("ferdinandApiToken"),
                        'Content-Type': 'application/json'
                    }
                };

                $.ajax(ajaxOptions)
                    .success(function (data) {
                        self.sets.push(new Set(data.workoutSupersetExercise));
                    })
                    .error(function (data) {
                    });
            }

            self.deleteSet = function (set) {
                var data = {
                    WorkoutSupersetExerciseSetId: set.id
                };

                var ajaxOptions = {
                    method: "POST",
                    url: "http://api.ferdinand.com/api/setdelete",
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'X-Ferdy-ApiToken': localStorage.getItem("ferdinandApiToken"),
                        'Content-Type': 'application/json'
                    }
                };

                $.ajax(ajaxOptions)
                    .success(function(data) {
                        self.sets.remove(set);
                    })
                    .error(function(data) {
                    });
            }
        }

        return Exercise;
    });