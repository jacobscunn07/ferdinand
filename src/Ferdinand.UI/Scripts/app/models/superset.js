﻿define(["ko", "exercise"],
    function (ko, Exercise) {
        var superset = function (model) {
            var self = this;

            self.id = model.id;
            self.exercises = ko.observableArray(_.map(model.supersetExercises,
                function (data) { return new Exercise(data) }));

            self.getHtmlPanelId = function() {
                return "superset-panel_" + self.id;
            }

            self.getHtmlPanelHeadingId = function () {
                return "superset-panel-heading_" + self.id;
            }

            self.getHtmlPanelBodyId = function () {
                return "superset-panel-body_" + self.id;
            }

            self.createExercise = function () {
                var data = {
                    WorkoutSupersetId: self.id
                };

                var ajaxOptions = {
                    method: "POST",
                    url: "http://api.ferdinand.com/api/workoutsupersetexercise",
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'X-Ferdy-ApiToken': localStorage.getItem("ferdinandApiToken"),
                        'Content-Type': 'application/json'
                    }
                };

                $.ajax(ajaxOptions)
                    .success(function (data) {
                        self.exercises.push(new Exercise(data.workoutSupersetExercise));
                    })
                    .error(function (data) {
                    });
            }

            self.copyExercise = function (exercise) {
                var data = {
                    WorkoutSupersetId: self.id,
                    WorkoutSupersetExerciseId: exercise.id
                };

                var ajaxOptions = {
                    method: "POST",
                    url: "http://api.ferdinand.com/api/WorkoutSupersetExerciseCopy",
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'X-Ferdy-ApiToken': localStorage.getItem("ferdinandApiToken"),
                        'Content-Type': 'application/json'
                    }
                };

                $.ajax(ajaxOptions)
                    .success(function (data) {
                        self.exercises.push(new Exercise(data.workoutSupersetExercise));
                    })
                    .error(function (data) {
                    });
            }

            self.deleteExercise = function (exercise) {
                var data = {
                    WorkoutSupersetExerciseId: exercise.id
                };

                var ajaxOptions = {
                    method: "POST",
                    url: "http://api.ferdinand.com/api/WorkoutSupersetExerciseDelete",
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'X-Ferdy-ApiToken': localStorage.getItem("ferdinandApiToken"),
                        'Content-Type': 'application/json'
                    }
                };

                $.ajax(ajaxOptions)
                    .success(function(data) {
                        self.exercises.remove(exercise);
                    })
                    .error(function(data) {
                    });
            }
        }

        return superset;
    });