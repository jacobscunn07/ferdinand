﻿define(["ko", "superset", "_"],
    function (ko, Superset, _) {
        var Workout = function(model) {
            var self = this;

            self.id = model.id;
            self.name = ko.observable(model.name);
            self.description = ko.observable(model.description);
            self.supersets = ko.observableArray(_.map(model.supersets, function (data) { return new Superset(data) }));

            self.createSuperset = function () {
                var data = {
                    WorkoutId: self.id
                };

                var ajaxOptions = {
                    method: "POST",
                    url: "http://api.ferdinand.com/api/AddSuperset",
                    dataType: 'json',
                    data: JSON.stringify(data),
                    headers: {
                        'X-Ferdy-ApiToken': localStorage.getItem("ferdinandApiToken"),
                        'Content-Type': 'application/json'
                    }
                };

                $.ajax(ajaxOptions)
                    .success(function (data) {
                        self.supersets.push(new Superset(data.workoutSuperset));
                    })
                    .error(function (data) {
                    });
            }

            self.deleteSuperset = function(superset) {
                self.supersets.remove(superset);
            }
        }

        return Workout;
    });