﻿define(["ko"],
    function () {
        var Attribute = function (model) {
            var self = this;
            self.id = model.id;
            self.dataType = model.attributeDataType;
            self.label = model.attributeName;
            self.value = ko.observable(model.value);
            return self;
        }

        var Set = function(model) {
            var self = this;

            self.id = model.id;
            self.attributes = ko.observableArray(_.map(model.supersetExerciseSetAttributes,
                function (data) { return new Attribute(data) }));

            self.getHtmlPanelId = function () {
                return "set-panel_" + self.id;
            }

            self.getHtmlPanelHeadingId = function () {
                return "set-panel-heading_" + self.id;
            }

            self.getHtmlPanelBodyId = function () {
                return "set-panel-body_" + self.id;
            }
        }

        return Set;
    });