﻿using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Features.Account;
using Ferdinand.Core.Security;
using Should.Core.Assertions;
using System.Linq;

namespace Ferdinand.Tests.Features.Account
{
    public class RegisterCommandHandlerTests
    {
        private readonly RegisterCommandHandler _handler;
        private readonly FerdinandContext _context;

        public RegisterCommandHandlerTests()
        {
            _context = FerdinandContext.Create();
            _handler = new RegisterCommandHandler(_context, new Salt(), new HashPassword());
        }

        public void ShouldRegisterUser()
        {
            var id = _handler.Handle(new RegisterCommand
            {
                Email = "Test@Test.com",
                Password = "abc123456",
                PhoneNumber = "2815430601"
            });

            var person = _context.People.Where(x => x.Id.Equals(id));
            Assert.Equal(1, person.Count());
        }
    }
}