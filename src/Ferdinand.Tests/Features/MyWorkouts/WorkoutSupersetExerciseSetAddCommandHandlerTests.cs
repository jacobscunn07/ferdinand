﻿using System;
using System.Data.Entity.Infrastructure;
using Ferdinand.Core.DataAccess;
using Ferdinand.Core.Features.Workouts.Supersets.Exercises.Sets;
using Should;

namespace Ferdinand.Tests.Features.MyWorkouts
{
    public class WorkoutSupersetExerciseSetAddCommandHandlerTests
    {
        private readonly SetAddCommandHandler _handler;
        private readonly FerdinandContext _ctx;

        public WorkoutSupersetExerciseSetAddCommandHandlerTests()
        {
            _ctx = FerdinandContext.Create();
            _handler = new SetAddCommandHandler(_ctx);
        }

        public void ShouldSuccessfullyAddExerciseSet()
        {
            var command = new SetAddCommand { WorkoutSupersetExerciseId = Guid.Parse("10864CD5-B7FF-4F69-96F2-6D03FAB5F87E") };
            var result = _handler.Handle(command);
            result.WorkoutSupersetExerciseSet.ShouldNotBeNull();
            result.WorkoutSupersetExerciseSet.Id.ShouldNotEqual(Guid.Empty);
        }

        public void ShouldNotSuccessfullyAddExerciseSet()
        {
            var command = new SetAddCommand { WorkoutSupersetExerciseId = Guid.Empty };

            try
            {
                var result = _handler.Handle(command);
            }
            catch (DbUpdateException e)
            {

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
