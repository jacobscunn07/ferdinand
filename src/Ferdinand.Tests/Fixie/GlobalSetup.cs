﻿using AutoMapper;
using Ferdinand.Core.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ferdinand.Tests.Fixie
{
    public static class GlobalSetup
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg => cfg.AddProfile(new QueryAutoMapperProfile()));
        }
    }
}
