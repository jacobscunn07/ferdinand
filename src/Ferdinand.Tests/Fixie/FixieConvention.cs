﻿using Ferdinand.Core.DependencyResolution;
using Fixie;
using StructureMap;
using System;
using System.Transactions;

namespace Ferdinand.Tests.Fixie
{
    public class FixieConvention : Convention
    {

        public FixieConvention()
        {
            Classes.NameEndsWith("Tests");

            Methods.Where(method => method.IsVoid());

            CaseExecution.Wrap<Transaction>();

            GlobalSetup.Initialize();
        }
    }

    class Transaction : CaseBehavior
    {
        public void Execute(Case context, Action next)
        {
            using (new TransactionScope())
                next();
        }
    }

    
}