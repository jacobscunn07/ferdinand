﻿using Should;

namespace Ferdinand.Tests
{
    public class CalculatorTests
    {
        public void ShouldAdd()
        {
            var calculator = new Calculator();
            calculator.Add(2, 3).ShouldEqual(5);
        }
    }

    public class Calculator
    {
        public double Add(double a, double b)
        {
            return a + b;
        }
    }
}