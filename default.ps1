Import-Module WebAdministration

Framework "4.5.2"

$projectName = "Ferdinand"
$configuration = 'Debug'
$basedir = resolve-path '.\'
$src = "$basedir\src"
$webAppProjectDir = "$src\Ferdinand.UI"
$deployDir = "$src\Ferdinand.UI.Deploy"
$deployPackagePathAndFileName = "$deployDir\Ferdinand.UI-$configuration.zip"
$webDeployExePath = "C:\Program Files\IIS\Microsoft Web Deploy V3\"
    
$database_instance = "(local)"
$database_name = "Ferdinand"
$scripts_path = "$src\Ferdinand.Database\Database\Ferdinand"

$rh_path = "$src\packages\roundhouse.0.8.6\bin"
$rh_exe = "$rh_path\rh.exe"
$rh_output_path = "$rh_path\output"
$rh_cmd_timeout = 600
$rh_version_file = "$src\Ferdinand.Core\bin\Debug\Ferdinand.Core.dll"

$publish_dir = "$basedir\Publish"

$sln = "$src\Ferdinand.sln"
$ENV = "LOCAL"

task default -depends Rebuild-Test-Database,Run-Tests
task ci -depends Rebuild-Test-Database,Run-Tests,Create-Deploy-Package

task Compile {
    exec { msbuild /t:clean /v:q /nologo /p:Configuration=$configuration $sln }
    exec { msbuild /t:build /v:q /nologo /p:Configuration=$configuration $sln }
}

task Rebuild-Database -depends Compile {    
    exec { &$rh_Exe -s $database_instance -d $database_name -vf $rh_version_file --env $ENV --silent -drop -o $rh_output_path --ct $rh_cmd_timeout }
    exec { &$rh_Exe -s $database_instance -d $database_name -f $scripts_path -vf $rh_version_file --env $ENV --simple --silent -o $rh_output_path --ct $rh_cmd_timeout }
}

task Rebuild-Test-Database -depends Compile {
    $db_name = $database_name + "_Test"
    
    exec { &$rh_Exe -s $database_instance -d $db_name -vf $rh_version_file --env $ENV --silent -drop -o $rh_output_path --ct $rh_cmd_timeout }    
    exec { &$rh_Exe -s $database_instance -d $db_name -f $scripts_path -vf $rh_version_file --env $ENV --simple --silent -o $rh_output_path --ct $rh_cmd_timeout }
}

task Run-Tests -depends Compile {
    $fixieRunners = @(gci $src\packages -rec -filter Fixie.Console.exe)

    if ($fixieRunners.Length -ne 1) {
        throw "Expected to find 1 Fixie.Console.exe, but found $($fixieRunners.Length)."
    }

    $fixieRunner = $fixieRunners[0].FullName

    exec { & $fixieRunner "$src\Ferdinand.Tests\bin\$configuration\Ferdinand.Tests.dll" }
}

task Create-Deploy-Package {
    Remove-ThenAddFolder($deployDir)
    $csprojFile = "$webAppProjectDir\Ferdinand.UI.csproj"
    & msbuild $csprojFile /t:Package /p:Platform=AnyCPU /p:Configuration=Release /p:PackageLocation=$deployPackagePathAndFileName /v:q
	Write-Host "Deployment Artifact: $deployPackagePathAndFileName"
	Push-AppveyorArtifact "$deployPackagePathAndFileName"
    if($LASTEXITCODE -ne 0) {
        throw "Failed to create deploy package"
        exit 1
    }
}

task Apply-Dev-Environment-Settings {
	$siteName = "api.ferdinand.com"
	$appPoolPath = "IIS:\AppPools\" + $siteName
	$sitePath = "IIS:\Sites\" + $siteName
	$apiDirectoryPath = "$src\Ferdinand.API"

	if(!(Test-Path ($appPoolPath)))
	{
	     $appPool = New-Item ($appPoolPath)

	     $appPool.managedRuntimeVersion = "v4.0"
	     $appPool | Set-Item

	     #Display Updated AppPool Settings
	     "AppPool = " +$appPool
	     "UserName = " + $appPool.processModel.userName
	     "Password = " + $appPool.processModel.password
	     "Runtime = " + $appPool.managedRuntimeVersion
	}

	if (Test-Path($sitePath))
	{
	    return
	}

	#create the site
	$iisApp = New-Item $sitePath -bindings @{protocol="http";bindingInformation=":80:" + $siteName} -physicalPath $apiDirectoryPath
	$iisApp | Set-ItemProperty -Name "applicationPool" -Value $siteName
}

#helper methods
function Remove-ThenAddFolder([string]$name) {
    Remove-IfExists $name
    New-Item -Path $name -ItemType "directory"
}
 
function Remove-IfExists([string]$name) {
    if ((Test-Path -path $name)) {
        dir $name -recurse | where {!@(dir -force $_.fullname)} | rm
        Remove-Item $name -Recurse
    }
}